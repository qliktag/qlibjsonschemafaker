# QLibJsonSchemaFaker

## What is this repository for?
QLibJsonSchemaFaker is created fake json from jsonschema

### Specific changes in library
- following code needs to be searched and changed  
`var _props = requiredProperties.concat(extraProperties).slice(0, max);`
changes to 
`var _props = allProperties.concat(extraProperties).slice(0, max);`
- LIPSUM_WORDS 2124  main.cjs.js change to `('a b c d e f g h i j k l m n o p q r s t u v w x y z').split(' ');`
- new LIPSUM_WORDS2124 main.cjs.js change to `var LIPSUM_WORDS = ["dummy string"];`
- following needs to change to 0
`MIN_NUMBER: 0,
MAX_NUMBER: 0,
MIN_INTEGER: 0,
MAX_INTEGER: 0`

## Setting up
* Install NVM version - 0.33.8
* Install Node version using nvm - 10.16.3
* clone the repo, replace the node_modules where required and verify your changes if any

### upgrade
- remove complete code and npm install the latest version of json-schema-faker and apply changes as per requirement
